#include "Tile.h"
#include "stdafx.h"


Tile::Tile(TextureManager* Tex, int initX, int initY)
{
	TexMan = Tex;
	pos = sf::Vector2f(initX, initY);
	TileSprite = new sf::Sprite;
	TileTexture = TexMan->LoadTexture("../External/sprites/WhiteTile.png");
	GoalTexture = TexMan->LoadTexture("../External/sprites/AStar.png");
	FailTexture = TexMan->LoadTexture("../External/sprites/FailedTile.png");
	ClosedTexture = TexMan->LoadTexture("../External/sprites/ClosedTile.png");
	ObstacleTexture = TexMan->LoadTexture("../External/sprites/BlackTile.png");
	PathTexture = TexMan->LoadTexture("../External/sprites/PathTile.png");
	StartTexture = TexMan->LoadTexture("../External/sprites/StartTile.png");
	OpenTexture = TexMan->LoadTexture("../External/sprites/OpenTile.png");
	TileSprite->setTexture(*TileTexture);
	TileSprite->setPosition(pos);
	fvalue = 0;
	gvalue = 0;
	hvalue = 0;
	start = goal = obstacle = open = closed = false;
}

Tile::Tile()
{
}


Tile::~Tile()
{
}

void Tile::Draw(sf::RenderWindow& window)
{
	window.draw(*TileSprite);
}

void Tile::setFValue(int Fnew)
{
	fvalue = Fnew;
}

void Tile::setGValue(int Gnew)
{
	gvalue = Gnew;
}

void Tile::setHValue(int Hnew)
{
	hvalue = Hnew;
}

void Tile::setGoal()
{
	goal = true;
	TileSprite->setTexture(*GoalTexture);
}

void Tile::setObstacle()
{
	if (!start && !closed && !open && !goal)
	{
		obstacle = !obstacle;
		if (obstacle)
		{
			TileSprite->setTexture(*ObstacleTexture);
		}
		else
		{
			TileSprite->setTexture(*TileTexture);
		}
	}
}

void Tile::setClosed()
{
	open = false;
	closed = true;
	TileSprite->setTexture(*ClosedTexture);
}

void Tile::setOpened()
{
	open = true;
	closed = false; 
	TileSprite->setTexture(*OpenTexture);
}

void Tile::setStart()
{
	start = true;
	TileSprite->setTexture(*StartTexture);
}

void Tile::setPath()
{
	TileSprite->setTexture(*PathTexture);
}

void Tile::setFailed()
{
	TileSprite->setTexture(*FailTexture);
}

void Tile::setParent(Tile * parentTile)
{
	TileParent = parentTile;
}

void Tile::Reset()
{
	TileSprite->setTexture(*TileTexture);
	obstacle = false;
	goal = false;
	start = false;
	open = false;
	closed = false;
	fvalue = 0;
	gvalue = 0;
	hvalue = 0;
	TileParent = NULL;
}

int Tile::GetFValue()
{
	return fvalue;
}

int Tile::GetHValue()
{
	return hvalue;
}

int Tile::GetGValue()
{
	return gvalue;
}

int Tile::GetXPos()
{
	return pos.x;
}



int Tile::GetYPos()
{
	return pos.y;
}

bool Tile::GetGoal()
{
	return goal;
}

bool Tile::GetObstacle()
{
	return obstacle;
}

bool Tile::GetStart()
{
	return start;
}

bool Tile::GetClosed()
{
	return closed;
}

bool Tile::GetOpened()
{
	return open;
}

Tile * Tile::GetParent()
{
	return TileParent;
}

