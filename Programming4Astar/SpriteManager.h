#pragma once
#include "stdafx.h"

class SpriteManager
{
public:
	SpriteManager();
	~SpriteManager();
	sf::Sprite* CreateSprite(const std::string& p_filename, int p_x, int p_y, int p_w, int p_h); //Create a sprite with a set texture at the appointed values and size
	void DestroySprite(sf::Sprite* p_sprite);

private:
	std::vector<sf::Sprite*> m_sprites;
	std::map<std::string, sf::Texture> m_textureMap;

};
