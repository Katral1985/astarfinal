#include "AstarCalculator.h"



AstarCalculator::AstarCalculator(Tile* Tilemap[setup::GRIDWIDTH][setup::GRIDHEIGHT])
{
	for (int i = 0; i < setup::GRIDWIDTH; i++)
	{
		for (int j = 0; j < setup::GRIDHEIGHT; j++)
		{
			gridreference[i][j] = Tilemap[i][j];
		}
	}
}

std::vector<Tile*> AstarCalculator::CalculatePath(int startx, int starty, int endx, int endy)
{
	Tile* lasttile;
	Tile* parenttile;
	std::vector<Tile*> path;
	std::pair<int, int> currentPair;
	FindNodes(startx, starty, endx, endy);
	MoveToClosed(startx, starty);
	CalculateFGH(startx, starty, endx, endy);
	currentPair = FindLowestF();
	lasttile=AStarRecurse(currentPair.first, currentPair.second, endx, endy);
	if (lasttile == nullptr)
	{
		/*NoPathException();*/
		return path;
	}
	parenttile = lasttile;
	while (parenttile != parenttile->GetParent())
	{
		path.emplace_back(parenttile);
		parenttile = parenttile->GetParent();
	}
	return path;
}


AstarCalculator::~AstarCalculator()
{
}

void AstarCalculator::FindNodes(int indexX, int indexY, int endX, int endY)
{
	for (int i = -1; i <= 1 ; i++)
	{
		for (int j = -1; j <= 1; j++)
		{
			MoveToOpen((indexX + i), (indexY + j), endX, endY, gridreference[indexX][indexY]);
		}
	}
}

std::pair<int, int> AstarCalculator::FindLowestF()
{
	//TODO Add breakout condition for empty list
	std::pair<int, int> temppair;
	fCheck=500;
	for (auto it : openList)
	{
		if (openList.empty())
		{
			break;
		}
		if (it->GetFValue() < fCheck)
		{
			fCheck = it->GetFValue();
			temppair = std::make_pair(it->GetXPos()/setup::TILESIZE, it->GetYPos() / setup::TILESIZE);
		}
	}
	return temppair;
}

void AstarCalculator::MoveToOpen(int indexX, int indexY, int endX, int endY, Tile* parentTile)
{
	if (indexX < 0 || indexX >= setup::GRIDWIDTH || indexY < 0 || indexY >= setup::GRIDHEIGHT) //If the tile is out of bounds
	{
		return;
	}
	
	if (std::find(closedList.begin(), closedList.end(), gridreference[indexX][indexY]) != closedList.end()) //If the reference already exists in the closed list
	{
		return;
	}
	if (gridreference[indexX][indexY]->GetObstacle())
	{
		return;
	}
	if (std::find(openList.begin(), openList.end(), gridreference[indexX][indexY]) != openList.end()) //If the reference already exists in the open list
	{
		int tempG = gridreference[indexX][indexY]->GetGValue();
		CalculateFGH(indexX, indexY, endX, endY);
		
		//int parentX = gridreference[indexX][indexY]->GetParent()->GetXPos() / setup::TILESIZE;
		//int parentY = gridreference[indexX][indexY]->GetParent()->GetYPos() / setup::TILESIZE;
		//if (indexX != parentX && indexY != parentY)
		//{
		//	gridreference[indexX][indexY]->setGValue(gridreference[indexX][indexY]->GetParent()->GetGValue() + 14);
		//}
		//else if (indexX == parentX || indexY == parentY)
		//{
		//	gridreference[indexX][indexY]->setGValue(gridreference[indexX][indexY]->GetParent()->GetGValue() + 10);
		//}

		//gridreference[indexX][indexY]->setHValue((abs(endX - indexX) + abs(endY - indexY)) * 10);

		//gridreference[indexX][indexY]->setFValue(
		//	gridreference[indexX][indexY]->GetHValue() + gridreference[indexX][indexY]->GetGValue());
		if (gridreference[indexX][indexY]->GetGValue() >= tempG)
		{
			return;
		}
	}
	gridreference[indexX][indexY]->setParent(parentTile);
	openList.push_back(gridreference[indexX][indexY]);

}

void AstarCalculator::MoveToClosed(int indexX, int indexY)
{
	if (indexX < 0 || indexX >= setup::GRIDWIDTH || indexY < 0 || indexY >= setup::GRIDHEIGHT)
	{
		return;
	}
	if (std::find(closedList.begin(), closedList.end(), gridreference[indexX][indexY]) != closedList.end())
	{
		return;
	}
	if (std::find(openList.begin(), openList.end(), gridreference[indexX][indexY]) != openList.end())
	{
		openList.erase(std::find(openList.begin(), openList.end(), gridreference[indexX][indexY]));
	}
	closedList.push_back(gridreference[indexX][indexY]);
	 //TODO change the affected tile to closed as well

}

Tile* AstarCalculator::AStarRecurse(int startx, int starty, int endx, int endy)
{
	std::pair<int,int> currentPair;
	FindNodes(startx, starty, endx, endy);
	MoveToClosed(startx, starty);
	currentPair=FindLowestF();
	if (currentPair.first == endx && currentPair.second == endy)
	{
		return gridreference[currentPair.first][currentPair.second];
	}
	if (openList.empty())
	{
		return nullptr; //If there is no path to take, return null
	}
	AStarRecurse(currentPair.first, currentPair.second, endx, endy);
}

void AstarCalculator::CalculateFGH(int currentX, int currentY, int endX, int endY)
{
	int parentX = gridreference[currentX][currentY]->GetParent()->GetXPos() / setup::TILESIZE;
	int parentY = gridreference[currentX][currentY]->GetParent()->GetYPos() / setup::TILESIZE;
	if (currentX != parentX && currentY != parentY)
	{
		gridreference[currentX][currentY]->setGValue(gridreference[currentX][currentY]->GetParent()->GetGValue() + 14);
	}
	else if (currentX == parentX || currentY == parentY)
	{
		gridreference[currentX][currentY]->setGValue(gridreference[currentX][currentY]->GetParent()->GetGValue() + 10);
	}

	gridreference[currentX][currentY]->setHValue((abs(endX - currentX) + abs(endY - currentY))*10);

	gridreference[currentX][currentY]->setFValue(
	gridreference[currentX][currentY]->GetHValue() + gridreference[currentX][currentY]->GetGValue()
	);

}

void AstarCalculator::NoPathException()
{
	std::cout << "Astar failed to find a path, reset and try again!" << std::endl;
	for (int i = 0; i < closedList.size(); i++)
	{
		closedList.at(i)->setFailed();
	}
}

