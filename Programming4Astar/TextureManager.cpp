#include "TextureManager.h"



TextureManager::TextureManager()
{
}


TextureManager::~TextureManager()
{
	auto iter = textures.begin();
	while (iter != textures.end())
	{
		delete (*iter).second;
		iter++;
	}
	textures.clear();
}

void TextureManager::PreloadTextures()
{
	CreateTexture("../External/sprites/WhiteTile.png");
	CreateTexture("../External/sprites/AStar.png");
	CreateTexture("../External/sprites/ClosedTile.png");
	CreateTexture("../External/sprites/Character.png");
	CreateTexture("../External/sprites/FailedTile.png");
	CreateTexture("../External/sprites/PathTile.png");
	CreateTexture("../External/sprites/BlackTile.png");
	CreateTexture("../External/sprites/OpenTile.png");
	CreateTexture("../External/sprites/StartTile.png");

}

void TextureManager::CreateTexture(std::string filePath)
{
	auto iter = textures.find(filePath);
	if (iter == textures.end())
	{
		sf::Texture* tempTex = new sf::Texture;
		if (!tempTex->loadFromFile(filePath))
		{
			std::cout << "Failed to load texture from "<< filePath << std::endl;
		}
		textures.insert(std::pair<std::string, sf::Texture*>(filePath, tempTex));
		iter = textures.find(filePath);

	}
}

sf::Texture* TextureManager::LoadTexture(std::string filepath)
{
	auto iter = textures.find(filepath);
	sf::Texture* tempTex = iter->second;
	return tempTex;
}


